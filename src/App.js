import './App.css';
import Variables from './Variables';

function App() {
  return (
    <div className="App">
      <header className="App-header">
      <p>Straatnaam: <input type="text" id="streetName"></input></p>
      <p>Straatnummer: <input type="text" id="streetNummer"></input></p>
      <p>Postcode: <input type="text" id="postcode"></input></p>
      <p>Plaats: <input type="text" id="plaats"></input></p>
      {Variables}
      <button onclick="Variables()">Zoek</button>

      <p id="demo1"></p>
      <p id="demo2"></p>
      <p id="demo3"></p>
      <p id="demo4"></p>
      </header>
    </div>
  );
}

export default App;
